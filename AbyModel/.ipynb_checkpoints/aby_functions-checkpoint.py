import numpy as np
import pandas as pd
from keras.models import load_model
from joblib import dump, load
import matplotlib.pyplot as plt
import keras
from datetime import datetime, timedelta
import os

print(os.getcwd())

class AbyModel:
    
    def __init__(self, type_model:str or int):
        
        self.type_model = type_model
        
        # cargar modelo
        self.__aby_load_model()   
        
        # Predicciones
        self.__aby_prediction()    
        
        # Reescalado de datos
        self.__aby_transform_data()
        
        # Obtener metricas
        self.__aby_metrics()
        
        # Grafico
        self.__aby_plot_all_serie()
        
        # Grafico de prueba
        self.__aby_plot_test_serie()
        
    def __aby_load_model(self):
        self.type_model == int(self.type_model)    
        print(os.getcwd())
        self.date_train = np.load(os.path.join(os.getcwd(), 'data', 'date_tr.npy'))
        self.date_validation = np.load(os.path.join(os.getcwd(), 'data', 'date_vl.npy'))
        self.date_test = np.load(os.path.join(os.getcwd(),  'data', 'date_ts.npy'))
        
        if self.type_model == 7:
            # Modelo
            self.model = load_model(os.path.join(os.getcwd(), 'model', 'sevenModel.h5'))
            # Conjunto de entrenamiento
            self.x_tr_s = np.load(os.path.join(os.getcwd(), 'data', 'x_tr_s_seven.npy'))
            # Conjunto de validación
            self.x_vl_s = np.load(os.path.join(os.getcwd(), 'data', 'x_vl_s_seven.npy'))
            # Conjunto de prueba
            self.x_ts_s = np.load(os.path.join(os.getcwd(), 'data', 'x_ts_s_seven.npy'))
            # Conjunto y de entrenamiento
            self.y_tr_s = np.load(os.path.join(os.getcwd(), 'data', 'y_tr_s_seven.npy'))
            # Conjunto y de validacion
            self.y_vl_s = np.load(os.path.join(os.getcwd(),'data', 'y_vl_s_seven.npy'))
            # Conjunto y de prueba
            self.y_ts_s = np.load(os.path.join(os.getcwd(),'data', 'y_ts_s_seven.npy'))
            # Escaler
            self.scaler = load(os.path.join(os.getcwd(), 'data', 'scaler_seven.pkl'))
        
        elif self.type_model == 15:
            # Modelo
            self.model = load_model(os.path.join(os.getcwd(), 'model', 'fifteenModel.h5'))
            # Conjunto de entrenamiento
            self.x_tr_s = np.load(os.path.join(os.getcwd(), 'data', 'x_tr_s_fifteen.npy'))
            # Conjunto de validación
            self.x_vl_s = np.load(os.path.join(os.getcwd(), 'data', 'x_vl_s_fifteen.npy'))
            # Conjunto de prueba
            self.x_ts_s = np.load(os.path.join(os.getcwd(), 'data', 'x_ts_s_fifteen.npy'))
            # Conjunto y de entrenamiento
            self.y_tr_s = np.load(os.path.join(os.getcwd(), 'data', 'y_tr_s_fifteen.npy'))
            # Conjunto y de validacion
            self.y_vl_s = np.load(os.path.join(os.getcwd(),'data', 'y_vl_s_fifteen.npy'))
            # Conjunto y de prueba
            self.y_ts_s = np.load(os.path.join(os.getcwd(),'data', 'y_ts_s_fifteen.npy'))
            # Escaler
            self.scaler = load(os.path.join(os.getcwd(), 'data', 'scaler_fifteen.pkl'))
        
        elif self.type_model == 30:
            # Modelo
            self.model = load_model(os.path.join(os.getcwd(), 'model', 'thirtyModel.h5'))
            # Conjunto de entrenamiento
            self.x_tr_s = np.load(os.path.join(os.getcwd(), 'data', 'x_tr_s_thirty.npy'))
            # Conjunto de validación
            self.x_vl_s = np.load(os.path.join(os.getcwd(), 'data', 'x_vl_s_thirty.npy'))
            # Conjunto de prueba
            self.x_ts_s = np.load(os.path.join(os.getcwd(), 'data', 'x_ts_s_thirty.npy'))
            # Conjunto y de entrenamiento
            self.y_tr_s = np.load(os.path.join(os.getcwd(), 'data', 'y_tr_s_thirty.npy'))
            # Conjunto y de validacion
            self.y_vl_s = np.load(os.path.join(os.getcwd(),'data', 'y_vl_s_thirty.npy'))
            # Conjunto y de prueba
            self.y_ts_s = np.load(os.path.join(os.getcwd(),'data', 'y_ts_s_thirty.npy'))
            # Escaler
            self.scaler = load(os.path.join(os.getcwd(), 'data', 'scaler_thirty.pkl'))
        
    def __aby_transform_data(self):
        self.data_train_pred = self.scaler.inverse_transform(self.data_train_pred)
        self.data_validation_pred = self.scaler.inverse_transform(self.data_validation_pred)
        self.data_test_pred = self.scaler.inverse_transform(self.data_test_pred)
        
        self.y_data_train_ = self.scaler.inverse_transform(self.y_tr_s[:, :, 0])
        self.y_data_validation_ = self.scaler.inverse_transform(self.y_vl_s[:, :, 0])
        self.y_data_test_ = self.scaler.inverse_transform(self.y_ts_s[:, :, 0])
        
    def __aby_prediction(self):
        self.data_train_pred = self.model.predict(self.x_tr_s, verbose=0)
        self.data_validation_pred = self.model.predict(self.x_vl_s, verbose=0)
        self.data_test_pred = self.model.predict(self.x_ts_s, verbose=0)
        
    def __aby_plot_error(self):
        self.errores = self.y_data_test_[:, 0]-self.data_test_pred[:, 0]
        plt.plot(self.errores);
        plt.show()
         
    def __aby_metrics(self):
        rmse_tr = self.model.evaluate(x=self.x_tr_s , y=self.y_tr_s, verbose=0)
        rmse_vl = self.model.evaluate(x=self.x_vl_s, y=self.y_vl_s, verbose=0)
        rmse_ts = self.model.evaluate(x=self.x_ts_s, y=self.y_ts_s, verbose=0)

        print('MSE:')
        print(f'    MSE train:\t {rmse_tr:.5f}')
        print(f'    MSE val:\t {rmse_vl:.5f}')
        print(f'    MSE test:\t {rmse_ts:.5f}')
        
        print("\nGráfico de errores\n")
        self.__aby_plot_error()
        
        # Máscara para errores menores a 0
        errores_menores_a_cero = self.errores[self.errores < 0]

        # Máscara para errores mayores a 0
        errores_mayores_a_cero = self.errores[self.errores > 0]

        # Calculamos los promedios
        promedio_errores_menores_a_cero = np.mean(errores_menores_a_cero)
        promedio_errores_mayores_a_cero = np.mean(errores_mayores_a_cero)

        # Imprimimos los resultados
        print(f"   Promedio de errores menores a 0: {promedio_errores_menores_a_cero}")
        print(f"   Promedio de errores mayores a 0: {promedio_errores_mayores_a_cero}\n")
        
    def __aby_plot_config(self):
        
        plt.xlabel("Fecha")
        plt.ylabel("Valor")
        plt.title("Predicciones vs datos reales")
        plt.legend()
        plt.grid(True)
        plt.tight_layout()
        plt.show()     
        
    def __aby_plot_all_serie(self):
        
        print(f"Grafico: datos reales vs datos predichos (entrenamiento, validacion y prueba)")
        
        plt.figure(figsize=(12, 6))

        # Gráfica de entrenamiento
        plt.plot(self.date_train[:self.data_train_pred[:, :].shape[0]], self.y_data_train_[:, 0], color='blue', label="Training (Real)")
        plt.plot(self.date_train[:self.data_train_pred[:, :].shape[0]], self.data_train_pred[:, 0], '--', color='cyan', label="Training (Predicción)")

        # Gráfica de validación
        plt.plot(self.date_validation[:self.data_validation_pred[:, :].shape[0]], self.y_data_validation_[:, 0], color='green', label="Validación (Real)")
        plt.plot(self.date_validation[:self.data_validation_pred[:, :].shape[0]], self.data_validation_pred[:, 0], '--', color='yellow', label="Validación (Predicción)")

        # Gráfica de prueba
        plt.plot(self.date_test[:self.data_test_pred[:, :].shape[0]], self.y_data_test_[:, 0], color='red', label="Test (Real)")
        plt.plot(self.date_test[:self.data_test_pred[:, :].shape[0]], self.data_test_pred[:, 0], '--', color='orange', label="Test (Predicción)")
        
        self.__aby_plot_config()
        
    def __aby_plot_test_serie(self):
        
        print(f"\nGrafico: datos reales vs datos predichos (prueba)")       
        
        plt.figure(figsize=(12, 6))

         # Gráfica de prueba
        plt.plot(self.date_test[:self.data_test_pred[:, :].shape[0]], self.y_data_test_[:, 0], color='red', label="Test (Real)")
        plt.plot(self.date_test[:self.data_test_pred[:, :].shape[0]], self.data_test_pred[:, 0], '--', color='orange', label="Test (Predicción)")

        self.__aby_plot_config()
        
    def aby_predicion_future(self, x_data):
        x_data = self.scaler.transform(x_data)
        self.prediction_data = self.model.predict(x_data, verbose=0)
        
        self.prediction_data = self.scaler.inverse_transform(x_data)
        
        plt.figure(figsize=(12, 6))
        
        # Tu fecha en formato de cadena
        fecha_str = str(self.date_test[-1])[:-3]

        # Convertir la cadena a objeto datetime
        fecha = datetime.strptime(fecha_str, '%Y-%m-%dT%H:%M:%S.%f')
        # Obtener una lista con los siguientes 7 días
        lista_proximos_7_dias = [fecha + timedelta(days=i) for i in range(8)][1:]
        
        plt.plot(lista_proximos_7_dias, self.prediction_data[0, :], color='red', label="Test (Real)")
        
        self.__aby_plot_config()
        
from sklearn.preprocessing import MinMaxScaler
import numpy as np

def standar_value_1_3(data_train_pred, y_data_train_, y_data_validation_, data_validation_pred, y_data_test_, data_test_pred):
    
    # Crear el objeto MinMaxScaler con el rango deseado
    scaler = MinMaxScaler(feature_range=(1, 3))

    # Ajustar a los datos y luego transformarlos
    data_train_pred = scaler.fit_transform(data_train_pred )
    # Ajustar a los datos y luego transformarlos
    y_data_train_ = scaler.transform(y_data_train_ )

    y_data_validation_ = scaler.transform(y_data_validation_ )
    data_validation_pred = scaler.transform(data_validation_pred )

    y_data_test_ = scaler.transform(y_data_test_ )
    data_test_pred = scaler.transform(data_test_pred )
    
    return data_train_pred, y_data_train_, y_data_validation_, data_validation_pred, y_data_test_, data_test_pred

def visualization_prediction_standar_values(aby_model, data_train_pred, y_data_train_, y_data_validation_, data_validation_pred, y_data_test_, data_test_pred):
    plt.figure(figsize=(12, 6))

    # Gráfica de entrenamiento
    plt.plot(aby_model.date_train[:aby_model.data_train_pred[:, :].shape[0]], y_data_train_[:, 0], color='blue', label="Training (Real)")
    plt.plot(aby_model.date_train[:aby_model.data_train_pred[:, :].shape[0]], data_train_pred[:, 0], '--', color='cyan', label="Training (Predicción)")

    # Gráfica de validación
    plt.plot(aby_model.date_validation[:aby_model.data_validation_pred[:, :].shape[0]], y_data_validation_[:, 0], color='green', label="Validación (Real)")
    plt.plot(aby_model.date_validation[:aby_model.data_validation_pred[:, :].shape[0]], data_validation_pred[:, 0], '--', color='yellow', label="Validación (Predicción)")

    # Gráfica de prueba
    plt.plot(aby_model.date_test[:aby_model.data_test_pred[:, :].shape[0]], y_data_test_[:, 0], color='red', label="Test (Real)")
    plt.plot(aby_model.date_test[:aby_model.data_test_pred[:, :].shape[0]], data_test_pred[:, 0], '--', color='orange', label="Test (Predicción)")

    plt.xlabel("Fecha")
    plt.ylabel("Valor")
    plt.title("Predicciones vs datos reales")
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.show() 
    
    plt.figure(figsize=(12, 6))

    # Gráfica de prueba
    plt.plot(aby_model.date_test[:aby_model.data_test_pred[:, :].shape[0]], y_data_test_[:, 0], color='red', label="Test (Real)")
    plt.plot(aby_model.date_test[:aby_model.data_test_pred[:, :].shape[0]], data_test_pred[:, 0], '--', color='orange', label="Test (Predicción)")

    plt.xlabel("Fecha")
    plt.ylabel("Valor")
    plt.title("Predicciones vs datos reales")
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.show()   