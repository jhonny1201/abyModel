# AbyModel

## Clonar el repositorio

```
git clone https://gitlab.com/jhonny1201/abyModel.git

```

## Instalar paquetes necesarios

```
cd abyModel\AbyModel
pip install -r requirements.txt

```

## Modelo 7x7

Para correr este modelo debe seguir los siguientes pasos:

- Dirigirse hasta el directorio ```abyModel\AbyModel\model7x7```
- Abrir el notebook ```model7x7.ipyng```
- Correr las líneas de código en el notebook

